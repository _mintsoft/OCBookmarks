package org.schabi.ocbookmarks;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.schabi.ocbookmarks.REST.model.Bookmark;
import org.schabi.ocbookmarks.listener.BookmarkListener;
import org.schabi.ocbookmarks.ui.TagsRecyclerViewAdapter;

import java.util.ArrayList;

import static android.os.Build.VERSION.SDK_INT;

public class EditBookmarkDialog {

    private static final String logTAG = "ocbookmarks";

    Bookmark mBookmark;
    String mTitle = "";
    String mUrl = "";
    ArrayList<String> mTagList = new ArrayList<>();

    TagsRecyclerViewAdapter mAdapter;

    private BookmarkListener mOnBookmarkChangedListener;

    public void newBookmark(final String title, final String url, ArrayList<String> tagList) {
        this.mTitle = title;
        this.mUrl = url;
        this.mTagList = tagList;
    }

    public AlertDialog getDialog(final Activity context, Bookmark b, BookmarkListener listener) {
        mOnBookmarkChangedListener = listener;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.edit_bookmark_dialog, null);
        final Toolbar toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        final EditText urlInput = (EditText) view.findViewById(R.id.urlInput);
        final EditText titleInput = (EditText) view.findViewById(R.id.titleInput);
        final EditText descriptionInput = (EditText) view.findViewById(R.id.descriptionInput);

        String dialogTitle = null;

        if(b == null) {
            dialogTitle = "Add bookmark";
            mBookmark = Bookmark.emptyInstance();
            mBookmark.setTitle(mTitle);
            mBookmark.setUrl(mUrl);
        } else {
            dialogTitle = "Edit bookmark";
            mBookmark = b;
        }
        urlInput.setText(mBookmark.getUrl());
        titleInput.setText(mBookmark.getTitle());
        descriptionInput.setText(mBookmark.getDescription());

        for(String tag : mBookmark.getTags()) {
            mTagList.add(tag);
        }

        toolbar.setTitle(dialogTitle);
        toolbar.setNavigationIcon(R.drawable.ic_close);
        toolbar.inflateMenu(R.menu.edit_bookmark_menu);

        final AlertDialog dialog = new AlertDialog.Builder(context)
                .setCancelable(true)
                .setView(view)
                .create();


        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        toolbar.setNavigationOnClickListener(v -> dialog.dismiss());

        toolbar.setOnMenuItemClickListener(item -> {
            //Here Actually we are saving Data to NextCloud
            if(item.getItemId() == R.id.save_menu) {
                mBookmark.setUrl(urlInput.getText().toString());
                mBookmark.setTitle(titleInput.getText().toString());
                mBookmark.setDescription(descriptionInput.getText().toString());

                if(mBookmark.getUrl().isEmpty()){
                    Toast.makeText(context, R.string.no_url_entered, Toast.LENGTH_SHORT).show();
                }
                else {
                    mBookmark.setTags(mTagList);
                    if (mOnBookmarkChangedListener != null) {
                        mOnBookmarkChangedListener.bookmarkChanged(mBookmark);
                    }
                    dialog.dismiss();
                }
                return true;
            }
            return false;
        });



        // setup recycler view
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.tag_recycler_view);
        mAdapter = new TagsRecyclerViewAdapter(context, mTagList);
        mAdapter.setOnTagDeletedListener(tag -> {
            int location = mTagList.indexOf(tag);
            mTagList.remove(tag);
            mAdapter.notifyItemRemoved(location);
        });
        mAdapter.setOnTagEditedListener((oldTag, newTag) -> {
            if(newTag.isEmpty()) {
                int location = mTagList.indexOf(oldTag);
                mTagList.remove(oldTag);
                mAdapter.notifyItemRemoved(location);
            }

            if (!newTag.equals(oldTag)) {
                int oldTagPos = mTagList.indexOf(oldTag);
                if (oldTagPos >= 0) {
                    mTagList.set(oldTagPos, newTag);
                    mAdapter.notifyItemChanged(oldTagPos);
                }
            }
        });
        recyclerView.setAdapter(mAdapter);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 2));


        ImageButton addNewTagButton = (ImageButton) view.findViewById(R.id.addNewTag);
        addNewTagButton.setOnClickListener(v -> {
            addNewTag(context);
        });

        fixTitlebarColor(toolbar, context);

        return dialog;
    }



    private void fixTitlebarColor(Toolbar toolbar, Context context) {
        int textColor = 0;
        if(SDK_INT <= 23) {
            textColor = Color.parseColor("#ffffffff");
        } else {
            textColor = context.getColor(R.color.editTitlebarTextColor);
        }
        toolbar.setTitleTextColor(textColor);
        TextView saveItem  = (TextView) toolbar.findViewById(R.id.save_menu);
        saveItem.setTextColor(textColor);

    }

    private void addNewTag(Context context) {
        final EditText editText = new EditText(context);
        android.app.AlertDialog dialog = new android.app.AlertDialog.Builder(context)
                .setTitle(R.string.new_tag)
                .setView(editText)
                .setPositiveButton(R.string.save, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mAdapter.addTag(editText.getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                }).show();
        dialog.getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        dialog.show();
    }

}
