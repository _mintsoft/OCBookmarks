package org.schabi.ocbookmarks.ui

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.fragment.app.Fragment
import org.schabi.ocbookmarks.R


/**
 * Use the [AboutFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AboutFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        val root = inflater.inflate(R.layout.fragment_about, container, false)

        root.findViewById<LinearLayout>(R.id.maintainer_das).setOnClickListener {
            startBrowser("https://gitlab.com/bisada/")
        }
        root.findViewById<LinearLayout>(R.id.maintainer_schabi).setOnClickListener {
            startBrowser("https://gitlab.com/derSchabi/")
        }
        root.findViewById<LinearLayout>(R.id.maintainer_niedermann).setOnClickListener {
            startBrowser("https://github.com/stefan-niedermann/")
        }
        root.findViewById<LinearLayout>(R.id.maintainer_nuesse).setOnClickListener {
            startBrowser("https://github.com/newhinton/")
        }


        root.findViewById<LinearLayout>(R.id.sourcecode).setOnClickListener {
            startBrowser("https://gitlab.com/bisada/OCBookmarks")
        }
        root.findViewById<LinearLayout>(R.id.issues).setOnClickListener {
            startBrowser("https://gitlab.com/bisada/OCBookmarks/issues")
        }
        root.findViewById<LinearLayout>(R.id.sourcecode).setOnClickListener {
            startBrowser("https://gitlab.com/bisada/OCBookmarks/-/blob/master/LICENSE.txt")
        }

        return root
    }

    companion object {
        @JvmStatic
        fun newInstance() = AboutFragment().apply {}
    }


    private fun startBrowser(url: String) {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(url)
        startActivity(i)
    }
}