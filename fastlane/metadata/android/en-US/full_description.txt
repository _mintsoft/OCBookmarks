A fully open-source Nextcloud Bookmark app.

An App for the Nextcloud/Owncloud Bookmark App. With this app you can add, edit, delete and view bookmarks that are synced with your Nextcloud.
You need to have the Bookmarks-App with minimal required version 0.10.1 installed and enabled on your Nextcloud-Instance.
If you need more information about the Nextcloud Bookmark app, you can follow <a href="https://apps.nextcloud.com/apps/bookmarks">this link</a>.

<b>Features:</b><ul>
<li>Organize your bookmarks with labels 🔖</li>
<li>Manage tags 🏷</li>
<li>Translated in many languages 🌎</li>
<li>Multiple accounts</li>
<li>☑ now showing bookmarks in your Folders 📁 </li>
<li>Nextcloud Single Sign On 🔊 🔥. Needs <a href='https://github.com/nextcloud/android'>nextcloud files</a> app as dependency.</li></ul>
